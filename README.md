Zanata
------

This role was previously maintained by:
* Alex Eng
* Ding-Yi Chen
* Marco Grigull
* Sean Flanigan
* Timo Trinks
* Will Thames

Now this role is in maintenance mode only.


The Zanata application deployment role.
The current method is to configure standalone-full.xml for the jboss and zanata deployment.


Dependencies
------------

* ansible >= 2.3 (for systemd and lineinfile path support)
* mysql setup (local or remote)
* java setup
* jbosseap7 setup
* https termination

Variables
---------
* zanata_enforce_matchingusernames:
  Enforce username to match the returned from openId server for new user registration.
  (Default: true)

* zanata_mt_service:
  Connect to optional Zanata Magpie Translation (MT) service.
  Following are required to enable Zanata MT:
  * url: URL to MT service
  * user: the MT service user
  * token: API key for the user

  (Default: undefined)

* zanata_requires_remote_database_host:
  If _yes_, then ansible will wait for remote database running,
  yet `systemctl start eap7-standalone` will NOT start the database;
  if _no_, then ansible do nothing
  (Default: no)

* zanata_security_domains:
  Specify the authentication methods for users to login.
  You need at least one, at most two security domains.
  Supported security domains are:
  * internal
  * kerberos
  * openid
  * saml2

  (Default: {})


Migration Tasks
---------------

If moving from another hosted setup the following items should be observed.

1) database (mandatory)
import the database fronm either routine nightly dump (stage) or from final manual dump (production)


2) Lucene indexes (Optional, preferred for prod)
archive the Lucene index from the old host and unpack on the new.  The directory is configured as property hibernate.search.default.indexBase and has been configured to "/var/lib/zanata/indexes" for some deplopyments.

This item is optional as the indexes can be regenerated; this can take some time to complete when there is a lot of data/documents.  End user performance is substandard and translation matrix memory functionaly is incomplete whilst index regeneration takes place.


3) The documents directory (Mandatory)
archive the documents directory and unpack on the new host.  The directory is configured as property java:global/zanata/files/document-storage-directory and has been configured as "/var/lib/zanata/files" for some previous deployments.


Example playbook
----------------

```
- hosts: zanata-standalone

  vars:
    activation_key: 1-zanata
    clamav_allow_user: 'jboss'
    zanata_security_domains:
      internal: {}
      openid:
        module_option:
          name: "providerURL"
          value: "http://id.fedoraproject.org/"

    zanata_mt_service:
      url: "https://your.mt.service/"
      user: "your-mt-username"
      token: "your-mt-api-key"


  roles:
  - rhn-satelite-client
  - firewall
  - java
  - jbosseap7
  - mysql
  - clamav
  - zanata
  - nginx
  - web-proxy
```


```
- hosts: zanata-mw

  vars:
    zanata_database_hostname: my.db.server.example.com
    zanata_database_name: zanata
    zanata_database_username: zanata
    zanata_database_password: topsecret
    server_name: "translate.example.com"
    java_max_memory: "2600m"
    java_min_memory: "2600m"
    java_rpm_version: 1.8.0.77
    java_default_version: "{{ java_rpm_version }}"
    zanata_version: 3.9.5-1

  roles:
  - java
  - jbossas
  - zanata
```



Backup strategy
---------------

The following items require backup:
* database
* documents directory
* lucene index (optional)


TODO
----
- test with alternative hosting topologies
